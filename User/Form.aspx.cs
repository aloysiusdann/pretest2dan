﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesPretestDataContext db = new DataClassesPretestDataContext())
            {
                ControllerCompany controllerCompany = new ControllerCompany(db);
                listCompany.Items.AddRange(controllerCompany.DropDownList());
                ControllerUser controllerUser = new ControllerUser(db);

                ControllerPosition controllerPosition = new ControllerPosition(db);
                listPosition.Items.AddRange(controllerPosition.DropDownList());


                var user = controllerUser.Cari(Convert.ToInt32(Request.QueryString["id"]));

                if (user != null)
                {
                    listCompany.SelectedValue = user.IDCompany.ToString();
                    listPosition.SelectedValue = user.IDPosition.ToString();
                    inputName.Text = user.Name;
                    inputAddress.Text = user.Address;
                    inputEmail.Text = user.Email;
                    inputTelephone.Text = user.Telephone;
                    inputUsername.Text = user.Username;
                    inputPassword.Text = user.Password;

                    btnOk.Text = "Update";

                }
                else
                {
                    btnOk.Text = "Add New";
                }
            }
        }
    }

    protected void BtnOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (DataClassesPretestDataContext db = new DataClassesPretestDataContext())
            {
                ControllerUser user = new ControllerUser(db);

                if (btnOk.Text == "Add New")
                {
                    user.Create(int.Parse(listCompany.SelectedValue), int.Parse(listPosition.SelectedValue), inputName.Text, inputAddress.Text, inputEmail.Text, inputTelephone.Text, inputUsername.Text, inputPassword.Text);
                }
                else if (btnOk.Text == "Update")
                {
                    user.Update(int.Parse(Request.QueryString["id"]), int.Parse(listCompany.SelectedValue), int.Parse(listPosition.SelectedValue), inputName.Text, inputAddress.Text, inputEmail.Text, inputTelephone.Text, inputUsername.Text, inputPassword.Text);
                }
                db.SubmitChanges();

                Response.Redirect("/User/Default.aspx");

            }
        }
    }
    protected void BtnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("/User/Default.aspx");
    }

}