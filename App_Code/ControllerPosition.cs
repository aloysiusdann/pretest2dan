﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ControllerPosition
/// </summary>
public class ControllerPosition : ClassBase
{
    public ControllerPosition(DataClassesPretestDataContext _db) : base(_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public TBPosition[] Data()
    {
        return db.TBPositions.ToArray();
    }

    //Create data
    public TBPosition Create(string name)
    {
        TBPosition position = new TBPosition
        {
            Name = name,
            UID = Guid.NewGuid(),
            CreatedBy = 1,
            CreatedAt = DateTime.Now
        };

        db.TBPositions.InsertOnSubmit(position);

        return position;
    }

    //Search data
    public TBPosition Cari(string UID)
    {
        return db.TBPositions.FirstOrDefault(x => x.UID.ToString() == UID);
    }

    //Update
    public TBPosition Update(string UID, string name)
    {
        var position = Cari(UID);

        if (position != null)
        {
            position.Name = name;

            return position;
        }
        else
            return null;
    }

    //Delete
    public TBPosition Delete(string UID)
    {
        var position = Cari(UID);

        if (position != null)
        {
            db.TBPositions.DeleteOnSubmit(position);
            db.SubmitChanges();

            return position;
        }
        else
            return null;
    }

    public void DropDownListPosition(DropDownList dropDownList)
    {
        dropDownList.DataSource = Data();
        dropDownList.DataValueField = "ID";
        dropDownList.DataTextField = "Name";
        dropDownList.DataBind();

        dropDownList.Items.Insert(0, new ListItem { Value = "0", Text = "-Pilih-" });
    }

    public ListItem[] DropDownList()
    {
        List<ListItem> listItems = new List<ListItem>();

        listItems.Add(new ListItem { Value = "0", Text = "-Pilih-" });

        listItems.AddRange(Data().Select(x => new ListItem
        {
            Value = x.ID.ToString(),
            Text = x.Name
        }));

        return listItems.ToArray();
    }
}