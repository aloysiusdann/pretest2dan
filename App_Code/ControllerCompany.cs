﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ControllerCompany
/// </summary>
public class ControllerCompany : ClassBase
{
    public ControllerCompany(DataClassesPretestDataContext _db) : base(_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    //Untuk menampilkan data & get data
    public TBCompany[] Data()
    {
        return db.TBCompanies.ToArray();
    }

    //Create data
    public TBCompany Create(string name, string address,string email, string telephone)
    {
        TBCompany company = new TBCompany
        {
            Name = name,
            Address = address,
            Email = email,
            Telephone = telephone,
            Flag = 1,
            CreatedBy = 1,
            UID = Guid.NewGuid(),
            CreatedAt = DateTime.Now,

        };

        db.TBCompanies.InsertOnSubmit(company);

        return company;
    }

    //Search data
    public TBCompany Cari(string UID)
    {
        return db.TBCompanies.FirstOrDefault(x => x.UID.ToString() == UID);
    }

    //Update
    public TBCompany Update(string UID,string name, string address, string email, string telephone)
    {
        var company = Cari(UID);

        if (company != null)
        {
            company.Name = name;
            company.Address = address;
            company.Email = email;
            company.Telephone = telephone;

            return company;
        }
        else
            return null;
    }

    //Delete
    public TBCompany Delete(string UID)
    {
        var company = Cari(UID);

        if (company != null)
        {
            var user = db.TBUsers.Where(x => x.IDCompany.ToString() == company.ID.ToString());
            var doc = db.TBDocuments.Where(x => x.IDCompany.ToString() == company.ID.ToString());
            if (user != null)
            {
                db.TBUsers.DeleteAllOnSubmit(user);
                db.SubmitChanges();
            }
            if (doc != null)
            {
                db.TBDocuments.DeleteAllOnSubmit(doc);
                db.SubmitChanges();
            }
            db.TBCompanies.DeleteOnSubmit(company);
            db.SubmitChanges();

            return company;
        }
        else return null;

    }


    public void DropDownListCompany(DropDownList dropDownList)
    {
        dropDownList.DataSource = Data();
        dropDownList.DataValueField = "ID";
        dropDownList.DataTextField = "Name";
        dropDownList.DataBind();

        dropDownList.Items.Insert(0, new ListItem { Value = "0", Text = "-Pilih-" });
    }

    public ListItem[] DropDownList()
    {
        List<ListItem> company = new List<ListItem>();

        company.Add(new ListItem { Value = "0", Text = "-Pilih-" });

        company.AddRange(Data().Select(x => new ListItem
        {
            Value = x.ID.ToString(),
            Text = x.Name
        }));

        return company.ToArray();
    }

}