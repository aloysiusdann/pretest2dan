﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ControllerCategory
/// </summary>
public class ControllerCategory : ClassBase
{
    public ControllerCategory(DataClassesPretestDataContext _db) : base(_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public TBDocument_Category[] Data()
    {
        return db.TBDocument_Categories.ToArray();
    }

    //Create data
    public TBDocument_Category Create(string name)
    {
        TBDocument_Category category = new TBDocument_Category
        {
            Name = name,
            UID = Guid.NewGuid(),
            CreatedBy = 1,
            CreatedAt = DateTime.Now
        };

        db.TBDocument_Categories.InsertOnSubmit(category);

        return category;
    }

    //Search data
    public TBDocument_Category Cari(string UID)
    {
        return db.TBDocument_Categories.FirstOrDefault(x => x.UID.ToString() == UID);
    }

    //Update
    public TBDocument_Category Update(string UID, string name)
    {
        var data = Cari(UID);

        if (data != null)
        {
            data.Name = name;

            return data;
        }
        else
            return null;
    }

    //Delete
    public TBDocument_Category Delete(string UID)
    {
        var data = Cari(UID);

        if (data != null)
        {
            db.TBDocument_Categories.DeleteOnSubmit(data);
            db.SubmitChanges();

            return data;
        }
        else
            return null;
    }

    public void DropDownListCategory(DropDownList dropDownList)
    {
        dropDownList.DataSource = Data();
        dropDownList.DataValueField = "ID";
        dropDownList.DataTextField = "Name";
        dropDownList.DataBind();

        dropDownList.Items.Insert(0, new ListItem { Value = "0", Text = "-Pilih-" });
    }

    public ListItem[] DropDownList()
    {
        List<ListItem> listItems = new List<ListItem>();

        listItems.Add(new ListItem { Value = "0", Text = "-Pilih-" });

        listItems.AddRange(Data().Select(x => new ListItem
        {
            Value = x.ID.ToString(),
            Text = x.Name
        }));

        return listItems.ToArray();
    }
}