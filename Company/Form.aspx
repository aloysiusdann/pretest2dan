﻿<%@ Page Title="" Language="C#" MasterPageFile="~/assets/MasterPage.master" AutoEventWireup="true" CodeFile="Form.aspx.cs" Inherits="Company_Form" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <div class="container shadow-sm p-3 mb-5 mt-10 bg-body-tertiary rounded">
        <div class="row justify-content-start">
            <div class="col mb-3">
                <label class="form-label" for="inputName"><strong>Nama Company</strong></label>
                <asp:TextBox ID="inputName" CssClass="form-control" runat="server" placeholder="Name"></asp:TextBox>
                <span class="text-danger"></span>
            </div>
    
            <div class="col mb-3">
                <label class="form-label" for="Telephone"><strong>Telephone</strong></label>
                <asp:TextBox ID="inputTelephone" CssClass="form-control" runat="server" placeholder="Telephone"></asp:TextBox>
                <span class="text-danger"></span>
            </div>

        </div>
        <div class="row">
            <div class="col mb-3">
                <label class="form-label" for="Email"><strong>Email</strong></label>
                <asp:TextBox ID="inputEmail" CssClass="form-control" AutoCompleteType="Email" runat="server" placeholder="test@mail.com"></asp:TextBox>
                <span class="text-danger"></span>
            </div>

            <div class="col mb-3">
                <label class="form-label" for="Address"><strong>Address</strong></label>
                <asp:TextBox ID="inputAddress" CssClass="form-control" runat="server" placeholder="Address" TextMode="MultiLine"></asp:TextBox>
                <span class="text-danger"></span>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="d-grid gap-2">
                    <asp:Button ID="btnExit" Text="Cancel" CssClass="btn btn-danger" runat="server" OnClick="BtnCancel_Click"/>
                </div>
            </div>
            <div class="col">
                <div class="d-grid gap-2">
                    <asp:Button ID="btnOk" CssClass="btn btn-success" runat="server"  OnClick="BtnOk_Click"/>
                </div>
            </div>
        </div>
     </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderJavascript" Runat="Server">
</asp:Content>

