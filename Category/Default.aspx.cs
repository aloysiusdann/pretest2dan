﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Category_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesPretestDataContext db = new DataClassesPretestDataContext())
            {
                DataClassesPretestDataContext cntxt = new DataClassesPretestDataContext();
                var results = from TBDocument_Category in cntxt.TBDocument_Categories select TBDocument_Category;
                repCategory.DataSource = results;
                repCategory.DataBind();

            }

        }
    }

    protected void repeaterCategory_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        using (DataClassesPretestDataContext db = new DataClassesPretestDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/Category/Form.aspx?uid=" + e.CommandArgument);
            }
            else if (e.CommandName == "Delete")
            {
                var category = db.TBDocument_Categories.FirstOrDefault(x => x.UID.ToString() == e.CommandArgument.ToString());

                ControllerCategory controllerCategory = new ControllerCategory(db);
                controllerCategory.Delete(e.CommandArgument.ToString());

                db.SubmitChanges();

                Response.Redirect("/Category/Default.aspx");
            }
        }
    }

}