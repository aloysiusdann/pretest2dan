﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Position_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataClassesPretestDataContext cntxt = new DataClassesPretestDataContext();
            var results = from TBPosition in cntxt.TBPositions select TBPosition;
            repPosition.DataSource = results;
            repPosition.DataBind();
        }
    }

    protected void repeaterPosition_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        using (DataClassesPretestDataContext db = new DataClassesPretestDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/Position/Form.aspx?uid=" + e.CommandArgument);
            }
            else if (e.CommandName == "Delete")
            {
                var company = db.TBPositions.FirstOrDefault(x => x.UID.ToString() == e.CommandArgument.ToString());

                ControllerPosition position = new ControllerPosition(db);
                position.Delete(e.CommandArgument.ToString());

                //db.TBCompanies.DeleteOnSubmit(company);
                db.SubmitChanges();

                Response.Redirect("/Position/Default.aspx");
            }
        }
    }

    protected void BtnDelete_Click(object sender, EventArgs e)
    {
        DataClassesPretestDataContext dbcompany = new DataClassesPretestDataContext();
        //company.Create(inputName.Value, inputAddress.Value, inputTelephone.Value);

        ControllerCompany company = new ControllerCompany(dbcompany);

        //company.Delete(e);

    }
}