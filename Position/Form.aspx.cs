﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Position_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesPretestDataContext db = new DataClassesPretestDataContext())
            {
                ControllerPosition position = new ControllerPosition(db);

                var data = position.Cari(Request.QueryString["uid"]);

                if (data != null)
                {
                    inputName.Text = data.Name;

                    btnOk.Text = "Update";

                }
                else
                {
                    btnOk.Text = "Add New";
                }
            }
        }
    }

    protected void BtnOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (DataClassesPretestDataContext db = new DataClassesPretestDataContext())
            {
                ControllerPosition position = new ControllerPosition(db);

                if (btnOk.Text == "Add New")
                {
                    position.Create(inputName.Text);
                }
                else if (btnOk.Text == "Update")
                {
                    position.Update(Request.QueryString["uid"], inputName.Text);
                }
                db.SubmitChanges();

                Response.Redirect("/Position/Default.aspx");

            }
        }
    }


    protected void BtnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Position/Default.aspx");
    }
}